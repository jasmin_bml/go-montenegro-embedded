module.exports = {
    productionSourceMap: false,
    publicPath: process.env.NODE_ENV === 'development' ? '/' : process.env.VUE_APP_BASE_URL
};
