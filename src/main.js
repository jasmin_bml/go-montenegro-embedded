import Vue from "vue";
import App from "./App.vue";
import vueCustomElement from 'vue-custom-element'
Vue.use(vueCustomElement);

Vue.config.productionTip = false;
import "./plugins/element-ui";

Vue.customElement('go-widget', App);
