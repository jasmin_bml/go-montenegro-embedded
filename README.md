# go-montenegro-embedded

## Compiles and hot-reloads for development
```
npm install
npm run serve
```

### Build process
Before build, set ```VUE_APP_BASE_URL``` in ```.env.prod``` file.
That should be an url of root folder where embedded will deploy.
```
VUE_APP_BASE_URL = https://test-server.com/
```
To build a project run shell script. Open cmd in root and run
```
build\build.sh
```
This will install dependecies and run build for you. 
On build success upload content of ```dist``` to server.
If you want to try app before upload you can run ```npx serve``` in dist folder.

### Embedding 

Add script to head 

```
<head>
    <script src="https://test-server.com/index.js"></script>
</head>
```

Then add ```<go-widget></go-widget>``` into the div you want to embedded form.
Form will take full width of parent div. 
