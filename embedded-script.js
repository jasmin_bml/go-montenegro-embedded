const { promisify } = require('util');
const readFile = promisify(require('fs').readFile);
const fs = require('fs');

(async() => {
    let innerCode = "";

    const html = await readFile('./dist/index.html', 'utf8');
    const linkStartPosition = html.indexOf('<link href');
    const linkEndPosition = html.indexOf('</head>');
    const linkText = html.substring(linkStartPosition, linkEndPosition);


    const scriptStartPosition = html.indexOf('<script');
    const scriptEndPosition = html.indexOf('</body>');
    const scriptText = html.substring(scriptStartPosition, scriptEndPosition);
    innerCode += `document.head.innerHTML = '${linkText}' + document.head.innerHTML;`;

    let bodyScripts = scriptText.split('</script>');
    bodyScripts.pop();

    let onBodyLoad = '';
    bodyScripts.forEach((script, index) => {
        const src = script.substring(script.indexOf('http'), script.length -1);
        onBodyLoad += `
           var script${index} = document.createElement('script');   
           script${index}.src = '${src}';
           document.body.appendChild(script${index});
           if(window.location.href.indexOf('motorhuset.com') !== -1) 
           setTimeout(function(){
               document.body.setAttribute('style', 'display:block!important');
           }, 500)
       `
    });
    innerCode += `
    document.addEventListener("DOMContentLoaded", function(event) {
        ${onBodyLoad}
         var viewport = document.querySelector("meta[name=viewport]");
        if(viewport) {
            viewport.setAttribute('content', 'width=device-width,initial-scale=1.0,user-scalable=no');
        } else {
          var metaTag = document.createElement('meta');
          metaTag.name = "viewport";
          metaTag.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0";
          document.getElementsByTagName('head')[0].appendChild(metaTag);
        }
    });`;

    fs.writeFile('./dist/index.js', innerCode, function(err) {
        if(err) throw err;
    });

    console.log('script added');

})();
